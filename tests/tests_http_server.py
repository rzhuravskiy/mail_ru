import pytest

from http_manager import send_request


def test_without_parameter():
    resp = send_request(data='', method='GET')
    assert resp.status_code == 400


def test_parameter_another_type():
    resp = send_request(data='number=abc', method='GET')
    assert resp.status_code == 400


def test_parameter_is_empty():
    resp = send_request(data='number=""', method='GET')
    assert resp.status_code == 400


def test_parameter_is_zero_number():
    resp = send_request(data='number=0', method='GET')
    valid_response_Odd = '{"result": {"number": "Zero is neither even, not odd."}}'
    assert 200 == resp.status_code
    assert valid_response_Odd == resp.text


def test_parameter_is_correct_odd():
    resp = send_request(data='number=3', method='GET')
    valid_response_Odd = '{"result": {"number": "Odd"}}'
    assert 200 == resp.status_code
    assert valid_response_Odd == resp.text


def test_parameter_is_correct_even():
    resp = send_request(data='number=4', method='GET')
    valid_response_Odd = '{"result": {"number": "Even"}}'
    assert 200 == resp.status_code
    assert valid_response_Odd == resp.text


def test_parameter_is_correct_minus():
    resp = send_request(data='number=-4', method='GET')
    valid_response_Odd = '{"result": {"number": "Even"}}'
    assert 200 == resp.status_code
    assert valid_response_Odd == resp.text


def test_parameter_is_float_even():
    resp = send_request(data='number=4.0', method='GET')
    valid_response_Odd = '{"result": {"number": "Even"}}'
    assert 200 == resp.status_code
    assert valid_response_Odd == resp.text


def test_parameter_is_float_odd():
    resp = send_request(data='number=4.1', method='GET')
    valid_response_Odd = '{"result": {"number": "Odd"}}'
    assert 200 == resp.status_code
    assert valid_response_Odd == resp.text


def test_invalid_query_method():
    resp = send_request(data='number=4', method='POST')
    assert 501 == resp.status_code

