import requests
import logging
from config import config

logger = logging.getLogger(__name__)


def send_request(data, method, service_name='/'):
    url = config['http_server']
    logger.info(
        'Sending %s request to %s with path and query %s%s  ' % (method, url, service_name, data))
    logger.debug('Equivalent: curl  %s%s -d \'%s\' -H ' % (url, service_name, data,))
    if method == 'GET':
        return requests.request(method=method, url=url, data=data, params=data)
    elif method == 'POST':
        return requests.request(method=method, url=url, data=data)

