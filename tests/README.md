**Запустить тесты:**

* 1) pip install -r requirements.txt 


* 2) pytest tests/tests_http_server.py


так же для примера добавил переменные в фикстуры **conftest.py**

тогда можно тесты свести к списку фикстур и одному тесту:


```python
def test_send_request_with_parameters(fixtures):
    resp = send_request(data=fixtures['data'], method=fixtures['method'])
    assert fixtures['status_code'] == resp.status_code
    assert fixtures['valid_response'] == resp.text
```


я решил что для небольшого кол-ва тестов это будет менее наглядно. 