import pytest


@pytest.fixture(params=[{

    'method': 'GET',
    'status_code': 400,
    'valid_response':'<head>\n<title>Error response</title>\n</head>\n<body>\n<h1>Error response</h1>\n<p>Error code 400.\n<p>Message: \nBad request , \nThe required parameter "number" must be of type numeric.\n<p>Error code explanation: 400 = Bad request syntax or unsupported method.\n</body>\n',
    'data': '',
    'name': 'test_parameter_is_empty'
},
    {
        'method': 'GET',
        'status_code': 400,
        'valid_response': '<head>\n<title>Error response</title>\n</head>\n<body>\n<h1>Error response</h1>\n<p>Error code 400.\n<p>Message: \nBad request , \nThe required parameter "number" must be of type numeric.\n<p>Error code explanation: 400 = Bad request syntax or unsupported method.\n</body>\n',
        'data': 'number=abc',
        'name': 'test_parameter_another_type'
    }
])
def fixtures(request):
    return request.param
