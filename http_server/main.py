from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from urlparse import urlparse, parse_qs
import json
import time

HOST_NAME = '0.0.0.0'
PORT_NUMBER = 5000


class Server(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_HEAD(self):
        self._set_headers()

    def do_GET(self):
        self._parse_request()

    def _parse_request(self):
        routers = {
            '/': self.parse_path_query}

        parsed_path = urlparse(self.path)
        router = routers.get(parsed_path.path)
        if router is None:
            self.send_error(404)
        router(parsed_path)

    def parse_path_query(self, parsed_path):
        if parsed_path.query:
            query = parse_qs(parsed_path.query)
            self.get_number_from_query(query)
        else:
            self.send_error(400, '\nBad request , \nThe required "number" parameter is missing\n')

    def get_number_from_query(self, query):
        if query.get('number', None):
            first_number = query['number'][0]
            if self.is_number(first_number):
                result = self.even_or_odd(first_number)
                self._set_headers()
                return self.wfile.write(json.dumps({'result': {'number': result}}))
            else:
                self.send_error(400, '\nBad request , \nThe required parameter "number" must be of type numeric')
        else:
            self.send_error(400, '\nBad request , \nThe required "number" parameter is missing\n')

    def even_or_odd(self, number):
        number = self.str_to_number(number)
        if number % 2 == 0 and number != 0:
            return 'Even'
        elif number == 0:
            return 'Zero is neither even, not odd.'
        else:
            return 'Odd'

    @staticmethod
    def str_to_number(number_in_str):
        if number_in_str.count('.') == 0:
            return int(number_in_str)
        else:
            return float(number_in_str)

    @staticmethod
    def is_number(str):
        try:
            float(str)
            return True
        except TypeError:
            return False
        except ValueError:
            return False


def run(server_class=HTTPServer, handler_class=Server):
    server_address = (HOST_NAME, PORT_NUMBER)
    httpd = server_class(server_address, handler_class)
    print time.asctime(), "Server Starts - http://%s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()
        print time.asctime(), "Server Stops - http://%s:%s" % (HOST_NAME, PORT_NUMBER)


if __name__ == "__main__":
    print "Server starting...."
    run()
