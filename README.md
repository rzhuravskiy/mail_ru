Написать микросервис:

- Висит на 80ом порту и принимает HTTP запросы

- При GET запросе  с параметром number, отвечает json’ом с результатом: четное число или нечетное

- При запросе отличным от GET, отбивает корректным кодом

- Если нет параметра number или содержимое не валидно, отбивает с корректным кодом
    (можно использовать, например, SimpleHTTPServer)

Функционально протестировать этот микросервис. (python + pytest)

Микросервис спрятать за nginx и обернуть в docker

Результатом работы должен быть image Docker контейнера и набор тестов

**Запустить http_server + nginx:**

1) docker-compose up -d

**Запустить тесты:**

1) pip install -r requirements.txt

2) pytest tests/tests_http_server.py

